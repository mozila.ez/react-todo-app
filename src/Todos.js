import React from 'react'

const Todos = ({todos,deleteTodo}) =>{

    const todoList = todos.length ? (
        todos.map(todo => {
            return (
                <div className="collection-item" key={todo.id}>
                    {/* <span> {todo.content} </span> <i className="material-icons" onClick={ () => {deleteTodo(todo.id)} }> close </i>  */}
                    <li className="collection-item">{todo.content} <a className="secondary-content"> <i className="material-icons" onClick={ () => {deleteTodo(todo.id)} }> close </i></a></li>
                </div>
            )
        })
    ) : (
        <p className="center"> You have no todo's left, yay! </p>
    )
    return (
        <div className="todos collection">
            {todoList}
        </div>
    )
}

export default Todos